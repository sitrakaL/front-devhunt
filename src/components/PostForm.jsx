import { useState } from "react";
import axios from "axios";
import { baseUrl, predictionUrl } from "../api/globalVariable";

const PostForm = () => {
    const [showModal, setShowModal] = useState(false);
    const [title, setTitle] = useState('')
    const [questions, setQuestions] = useState('')
    const [inputValue, setInputValue] = useState("");
    const [filteredOptions, setFilteredOptions] = useState([]);
    const [disable, setDisable] = useState(true)
    const [commentNom, setCommentNom] = useState("")
    const [commentPrenom, setCommentPrenom] = useState("")
    const [commentUserId, setCommentUserId] = useState("")
    
    const getPostQuestions = () => {
        axios.get(baseUrl + 'post-questions?populate=deep,2', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(response => {
                let myArray = [];
                let UserCommentArray = []
                response.data.data.forEach(element => {
                    let newObj = {
                        id: element.id,
                        title: element.attributes.title,
                        body: element.attributes.body,
                        solved: false,
                        createdAt: element.attributes.createdAt,
                        updatedAt: element.attributes.updatedAt,
                        commentaires: element.attributes.commentaire_id.data,
                        userId: element.attributes.user_id.data.id,
                        commmentUserId: commentUserId,
                        commentNom: commentNom,
                        commentPrenom: commentPrenom
                    }

                    for (const key in newObj.commentaires) {
                        axios.get('http://ec2-52-73-37-109.compute-1.amazonaws.com:1337/api/users?filters[$and][0][commentaire_id][id][$eq]=' + newObj.commentaires[key].id + '&populate=deep', {
                            headers: {
                                Authorization: `Bearer ${localStorage.getItem('token')}`
                            }
                        })
                            .then(response => {
                                newObj.commentaires[key].commentPrenom = response.data[0].prenom
                                newObj.commentaires[key].commentUserId = response.data[0].id
                                newObj.commentaires[key].commentNom = response.data[0].nom
                            })
                            .catch(error => {
                                console.error(error);
                            });
                    }
                    // newObj.commentaires.forEach((element) => {
                    //     getUserComment(element.id)
                    //     // let userComment = {
                    //     //     nom : commentUserId,
                    //     //     prenom : commentPrenom,
                    //     //     id : commentUserId
                    //     // }
                    //     // UserCommentArray.push}(userComment)
                    //     newObj.commentaires.commentUserId = commentUserId
                    // })

                    myArray.push(newObj)
                });
                setPosts(myArray)
                console.log(response.data.data)
            })
            .catch(error => {
                console.error(error);
            });
    }

    const questionner = () => {
        fetch("http://ec2-18-234-105-244.compute-1.amazonaws.com:5000/predict", {
            method: "POST",
            mode: 'no-cors',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                post_body: questions,
            })
        }).then(response => response.json()).then( data => console.log(data))
            .catch(error => {
                console.error(error);
            })
        // axios.post(predictionUrl + 'post-questions', {
        //     data: {
        //         title: title,
        //         body: questions,
        //         commentaire_id: {},
        //         solved: false,
        //         user_id: {
        //             id: JSON.parse(localStorage.getItem('session')).id
        //         },
        //         tag_id: {
        //             id: 0
        //         }
        //     }
        // }, {
        //     headers: {
        //         Authorization: `Bearer ${localStorage.getItem('token')}`
        //     }
        // })
        //     .then(response => {
        //         console.log(response.data);
        //     })
        //     .catch(error => {
        //         console.error(error);
        //     });
    }


    let options = ['java', 'javascript', 'python', 'php', 'excel', 'android', 'zavatra', 'perl']
    const handleInputChange = (event) => {
        setInputValue(event.target.value);
        const filteredOptions = options.filter((option) =>
            option.toLowerCase().includes(event.target.value.toLowerCase())
        );
        setFilteredOptions(filteredOptions);
    };

    const handleOptionClick = (option) => {
        setInputValue(option);
        setFilteredOptions([]);
    };

    return (
        <div className="flex bg-white rounded-md py-2 px-2 mx-2">
            <div className="flex-1 mt-2">Poser une question...</div>
            <div className="flex-2">
                <button onClick={() => setShowModal(true)} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full text-xl">+</button>
            </div>

            {
                showModal ? (
                    <>
                        <div
                            className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                        >
                            <div className="relative w-full my-6 max-w-2xl">
                                {/*content*/}
                                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                    {/*header*/}
                                    <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                        <h3 className="text-3xl font-semibold">
                                            Créer une publication
                                        </h3>
                                        <button
                                            className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                                            onClick={() => setShowModal(false)}
                                        >
                                            <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                                                ×
                                            </span>
                                        </button>
                                    </div>
                                    {/*body*/}
                                    <div className="relative p-6 flex-auto">
                                        <label className="block text-gray-700 text-sm font-bold mb-2 py-2" for="username">
                                            Titre
                                        </label>
                                        <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="titre" type="text" placeholder="Username" autoComplete="off"
                                            onChange={(event) => {
                                                setTitle(event.target.value); if (event.target.value != "") {
                                                    setDisable(false)
                                                }; if (event.target.value == "") { setDisable(true) }
                                            }} />
                                        <label className="block text-gray-700 text-sm font-bold mb-2 py-2" for="username">
                                            Contenu
                                        </label>
                                        <textarea id="message" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500" placeholder="Ecrivez ici"
                                            onChange={(event) => { setQuestions(event.target.value) }} disabled={disable}></textarea>
                                        <label className="block text-gray-700 text-sm font-bold mb-2 py-2" for="username">
                                            Tags
                                        </label>
                                        <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="titre" type="text" placeholder="Tags" autoComplete="off"
                                            value={inputValue} onChange={handleInputChange} disabled={disable} />
                                        {filteredOptions.length > 0 && (
                                            <>
                                                <div className={inputValue.length > 0 ? "grid grid-cols-4 display-none text-center" : "grid grid-cols-4 hidden text-center"} >
                                                    {filteredOptions.map((option) => (

                                                        <p className="cursor-pointer" key={option} onClick={() => handleOptionClick(option)}>
                                                            {option}
                                                        </p>
                                                    ))}
                                                </div>
                                            </>

                                        )}
                                    </div>
                                    {/*footer*/}
                                    <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                        <button
                                            className="text-red-500 background-transparent px-6 py-2 text-sm focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                            type="button"
                                            onClick={() => { setShowModal(false); setInputValue('') }}
                                        >
                                            Fermer
                                        </button>
                                        <button
                                            className="bg-blue-500 text-white active:bg-emerald-600 text-sm px-3 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                            type="button"
                                            onClick={() => { setShowModal(false); questionner(); setInputValue(''); window.location.reload}}
                                        >
                                            Valider
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                    </>
                ) : null
            }
        </div>
    )


}

export default PostForm