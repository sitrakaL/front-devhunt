import axios from "axios";
import Swal from "sweetalert2"
import { useState, useContext, useEffect } from "react";
import { baseUrl, linkSms } from "../api/globalVariable";

const SignUp = (props) => {
    const [disabled, setDisabled] = useState(true)
    const [username, setUserName] = useState('')
    const [email, setEmail] = useState('')
    const [matricule, setMatricule] = useState('')
    const [nom, setNom] = useState('')
    const [prenom, setPrenom] = useState('')
    const [profil, setProfil] = useState('')
    const [tel, setTel] = useState('')
    const [niveau, setNiveau] = useState('')
    const [password, setPassword] = useState('')
    const [file, setFile] = useState(null);
    const [errors, setErrors] = useState('')
    const [isLoading, setIsLoading] = useState(false)

    const handleClick = () => {
        props.handleSignUp();
    }

    const changeUsername = (e) => {
        setUserName(e.target.value)
    }

    const changeEmail = (e) => {
        setEmail(e.target.value)
    }

    const changeMatricule = (e) => {
        setMatricule(e.target.value)
    }

    const changeNom = (e) => {
        setNom(e.target.value)
    }

    const changePrenom = (e) => {
        setPrenom(e.target.value)
    }

    const changeTel = (e) => {
        setTel(e.target.value)
    }

    const changeNiveau = (e) => {
        setNiveau(e.target.value)
    }

    const changePassword = (e) => {
        setPassword(e.target.value)
    }


    const changeProfil = (event) => {
        setFile(event.target.files[0]);
    };

    const validation_formulaire = () => {
        let error = {}
        let hasErrors = false
        if (username == '') {
            error.username = 'Vous devez renseigner votre nom d\'utilisateur !'
            hasErrors = true
        }
        if (email == '') {
            error.email = 'Vous devez entrer votre email !'
            hasErrors = true
        }
        else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
            error.emailNotValid = "L'email que vous avez entré n'est pas valide !"
            hasErrors = true
        }
        if (matricule == '') {
            error.matricule = 'Vous devez entrer votre matricule !'
            hasErrors = true
        }
        if (nom == '') {
            error.nom = 'Vous devez entrer votre nom !'
            hasErrors = true
        }
        if (prenom == '') {
            error.prenom = 'Vous devez entrer votre prénoms !'
            hasErrors = true
        }
        if (tel == '') {
            error.numero_phone = 'Vous devez entrer votre numero de téléphone !'
            hasErrors = true
        }
        if (password == '') {
            error.password = 'Vous devez entrer votre mot de passe !'
            hasErrors = true
        }
        if (hasErrors == false) {
        sendData()
        }
        setErrors(error)
    }

    const sendData = () => {

        const codeLasa = Math.floor(Math.random() * 1000000).toString().padStart(6, '0');
        let formData = new FormData();
        formData.append('number', tel)
        formData.append('messageBody', 'Voici le code de validation de votre inscription dans StackEni : ' + codeLasa + ' !')

        let formDataf = new FormData()
        formDataf.append('files', file)
        axios.post('http://ec2-52-73-37-109.compute-1.amazonaws.com:1337/api/upload/', formDataf)
            .then(response => {
                setProfil(response.data[0].id)
            })
            .then(function(response){
                saveUser()
            })
            .catch(error => {
                console.error(error);
            });

        function saveUser() {
            let bodyFormData = new FormData();

            bodyFormData.append('username', username)
            bodyFormData.append('email', email)
            bodyFormData.append('matricule', matricule)
            bodyFormData.append('nom', nom)
            bodyFormData.append('prenom', prenom)
            bodyFormData.append('niveau', niveau)
            bodyFormData.append('numero_phone', tel)
            bodyFormData.append('password', password)
            bodyFormData.append('profil', profil)

            axios({
                method: 'POST',
                url: linkSms,
                data: formData,
                headers: { "Content-Type": "application/JSON" }
            }).then(function (response) {
                console.log('code envoyé')
            })
                .catch(function (error) {

                })

            Swal.fire({
                title: 'Entrez votre code de validation',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Valider',
                showLoaderOnConfirm: true,
                preConfirm: (code) => {
                    if (code == codeLasa) {
                        return "Réussi"
                    }
                    else {
                        Swal.showValidationMessage(
                            `Le code que vous avez entré est fausse !`
                        )
                    }
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        icon: 'success',
                        text: 'Votre inscription a été validé avec succès',
                        timer: 2000,
                        showConfirmButton: false
                    })
                        .then(function () {
                            axios.post(baseUrl + 'auth/local/register', bodyFormData)
                                .then(function (response) {
                                    window.location.href = '/home'
                                })
                        })
                }
            })
            bodyFormData.append('profil', profil)
            axios.post(baseUrl + 'auth/local/register', bodyFormData)
                .then(function (response) {
                    // window.location.href = '/home'
                    console.log(profil)
                })
                .catch(function (error) {
                    console.log(profil)
                    console.log(error)
                })
        }



        // axios.post('http://ec2-34-201-164-250.compute-1.amazonaws.com:1337/api/auth/local/register', bodyFormData)
        // .then(function(response){
        //     window.location.href = '/home'
        // })

    }

    const listUsers = () => {
        axios.get('http://ec2-52-73-37-109.compute-1.amazonaws.com:1337/api/users?populate=*', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        }).then(function (response) {
            console.log(response.data)
        }).catch(function (error) {
            console.log(error)
        })
    }

    return (
        <div className="w-full max-w-xl">
            <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                <div className="mb-4">
                    <label className="block text-gray-700 text-sm font-bold mb-2 py-2" for="username">
                        Nom d'utilisateur
                    </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Username" autoComplete="off" onChange={changeUsername} />
                    <small className="small_alert text-red-600">{errors.username}</small>
                    <label className="block text-gray-700 text-sm font-bold mb-2 pt-3" for="username">
                        E-mail
                    </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="email" type="email" placeholder="Email" autoComplete="off" onChange={changeEmail} />
                    <small className="small_alert text-red-600">{errors.emailNotValid}</small>
                    <small className="small_alert text-red-600">{errors.email}</small>
                    <label className="block text-gray-700 text-sm font-bold mb-2 pt-3" for="matricule">
                        Matricule
                    </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="matricule" type="text" placeholder="Matricule" autoComplete="off" onChange={changeMatricule} />
                    <small className="small_alert text-red-600">{errors.matricule}</small>
                    <div className="grid grid-cols-2">
                        <div>
                            <label className="block text-gray-700 text-sm font-bold mb-2 pt-3" for="nom">
                                Nom
                            </label>
                            <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="nom" type="text" placeholder="Nom" autoComplete="off" onChange={changeNom} />
                            <small className="small_alert text-red-600">{errors.nom}</small>
                        </div>
                        <div className="ml-3">
                            <label className="block text-gray-700 text-sm font-bold mb-2 pt-3" for="prenom">
                                Prénoms
                            </label>
                            <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="prenom" type="text" placeholder="Prénoms" autoComplete="off" onChange={changePrenom} />
                            <small className="small_alert text-red-600">{errors.prenom}</small>
                        </div>
                    </div>
                    <label className="block text-gray-700 text-sm font-bold mb-2 pt-3" for="tel">
                        Num. tel
                    </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="tel" type="text" placeholder="Numéro de téléphone" autoComplete="off" onChange={changeTel}
                        onKeyPress={(event) => {
                            const keyCode = event.keyCode || event.which;
                            const keyValue = String.fromCharCode(keyCode);
                            if (!(/0|1|2|3|4|5|6|7|8|9/.test(keyValue))) {
                                event.preventDefault();
                            }
                        }
                        } />
                    <small className="small_alert text-red-600">{errors.numero_phone}</small>
                    <label className="block text-gray-700 text-sm font-bold mb-2 pt-3" for="tel">
                        Niveau
                    </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="tel" type="text" placeholder="Niveau" autoComplete="off" onChange={changeNiveau} />
                </div>
                <div className="mb-6">
                    <label className="block text-gray-700 text-sm font-bold mb-2" for="password">
                        Mot de passe
                    </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder="Password" autoComplete="off" onChange={changePassword} />
                    <small className="small_alert text-red-600">{errors.password}</small>
                </div>
                <div className="mb-6">
                    <label className="block text-gray-700 text-sm font-bold mb-2" for="password">
                        Photo de profil
                    </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="profil" type="file" placeholder="Photo de profil" autoComplete="off" onChange={changeProfil} />
                </div>
                <div className="flex items-center justify-between">
                    <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" onClick={validation_formulaire} type="button">
                        Sign Up
                    </button>
                    <a className="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800" href="#" onClick={handleClick}
                    >
                        Sign In
                    </a>
                </div>
            </form>
        </div>
    );
}

export default SignUp