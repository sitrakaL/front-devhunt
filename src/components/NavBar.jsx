import React, { useState } from "react";
import { Transition } from "@headlessui/react";
import { AiOutlineUser } from "react-icons/ai"
import { TbLogout } from "react-icons/tb"
import { Link } from "react-router-dom"
import Swal from "sweetalert2";
import axios from "axios";
import { baseUrl } from "../api/globalVariable";

function Nav() {
    const [isOpen, setIsOpen] = useState(false);
    const [query, setQuery] = useState('')

    function recherchePost() {
        axios.get(baseUrl + 'post-questions?filters[$and][0][title][$eq]= ' + query).then(resp => {
            console.log(resp.data)
        }).catch(error => {
            console.log(error);
        })
    }
    return (
        <div>
            <nav className="bg-gray-800 z-40 fixed top-0 right-0 left-0">
                <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <div className="flex items-center justify-between h-16">
                        <div className="flex items-center">
                            <div className="flex shrink-0">
                                {/* <img
                                    className="h-8 w-8"
                                    src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg"
                                    alt="Workflow"
                                /> */}



                            </div>
                            <div className="flex z-40 w-full ml-64 text-center">
                                {/* <div className="ml-10 flex items-baseline space-x-4">
                                    <a
                                        href="#"
                                        className=" hover:bg-gray-700 text-white px-3 py-2 rounded-md text-sm font-medium"
                                    >
                                        Dashboard
                                    </a>

                                    <a
                                        href="#"
                                        className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium"
                                    >
                                        Team
                                    </a>

                                    <a
                                        href="#"
                                        className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium"
                                    >
                                        Projects
                                    </a>

                                    <a
                                        href="#"
                                        className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium"
                                    >
                                        Calendar
                                    </a>

                                    <a
                                        href="#"
                                        className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium"
                                    >
                                        Reports
                                    </a>
                                </div> */}
                                <input className="flex rounded py-2 mt-1 w-80 mr-4" type="text" onChange={(e) => { setQuery(e.target.value); recherchePost() }} />
                                {/* <p className="text-white outline-1 px-1 mt-2">Recherche</p> */}
                            </div>
                        </div>
                        <div className="-mr-2 gap-2 flex text-white">
                            <div className="flex text-white">
                                {/*<AiOutlineUser className="cursor-pointer text-3xl" />*/}
                                <Link to="/profile">
                                    <img className="object-fill w-8 h-8 rounded-full" src="https://media.techtribune.net/uploads/2020/07/one-piece-luffy-haki-training-wano-1228926-1280x0.jpeg" />
                                </Link>
                            </div>
                            <div className="-mr-2 flex text-white"><TbLogout className="text-3xl cursor-pointer"
                                onClick={() => {
                                    Swal.fire({
                                        title: 'Se déconnecter ?',
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        cancelButtonText: 'Annuler',
                                        confirmButtonText: 'Oui'
                                    }).then((result) => {
                                        if (result.isConfirmed) {
                                            localStorage.removeItem('session')
                                            localStorage.removeItem('token')
                                            window.location.href = "/"
                                        }
                                    })

                                }}
                            /></div>
                        </div>

                        <div className="-mr-2 flex md:hidden">
                            <button
                                onClick={() => setIsOpen(!isOpen)}
                                type="button"
                                className="bg-gray-900 inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                                aria-controls="mobile-menu"
                                aria-expanded="false"
                            >
                                <span className="sr-only">Open main menu</span>
                                {!isOpen ? (
                                    <svg
                                        className="block h-6 w-6"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="currentColor"
                                        aria-hidden="true"
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth="2"
                                            d="M4 6h16M4 12h16M4 18h16"
                                        />
                                    </svg>
                                ) : (
                                    <svg
                                        className="block h-6 w-6"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="currentColor"
                                        aria-hidden="true"
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth="2"
                                            d="M6 18L18 6M6 6l12 12"
                                        />
                                    </svg>
                                )}
                            </button>
                        </div>
                    </div>
                </div>

                <Transition
                    show={isOpen}
                    enter="transition ease-out duration-100 transform"
                    enterFrom="opacity-0 scale-95"
                    enterTo="opacity-100 scale-100"
                    leave="transition ease-in duration-75 transform"
                    leaveFrom="opacity-100 scale-100"
                    leaveTo="opacity-0 scale-95"
                >
                    {(ref) => (
                        <div className="md:hidden" id="mobile-menu">
                            <div ref={ref} className="px-2 pt-2 pb-3 space-y-1 sm:px-3">
                                <a
                                    href="#"
                                    className="hover:bg-gray-700 text-white block px-3 py-2 rounded-md text-base font-medium"
                                >
                                    Dashboard
                                </a>

                                <a
                                    href="#"
                                    className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
                                >
                                    Team
                                </a>

                                <a
                                    href="#"
                                    className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
                                >
                                    Projects
                                </a>

                                <a
                                    href="#"
                                    className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
                                >
                                    Calendar
                                </a>

                                <a
                                    href="#"
                                    className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
                                >
                                    Reports
                                </a>
                            </div>
                        </div>
                    )}
                </Transition>
            </nav>
        </div>
    );
}

export default Nav;
