import { AiOutlineMinus, AiOutlinePlus, AiOutlineCheck } from "react-icons/ai";
import { BsDot } from "react-icons/bs"



const PostCommentaires = ({ user_id, post, com, index }) => {

    return (
        <div className="flex flex-col my-2" key={index}>
            <div className="flex flex-row">
                <p className="font-bold pl-2"> {post.commentPrenom} {post.commentNom} </p>
                {/* <BsDot className="mt-1 ml-2" /> */}
                <p className="flex-1 text-gray-500 pl-2">{com.attributes.createdAt}</p>
                <div className="flex gap-2 mt-1">
                    <AiOutlineMinus className="cursor-pointer" />
                    <AiOutlinePlus className="cursor-pointer" />
                </div>
            </div>



            <div className="flex">
                {
                    (user_id === post.userId.user_id.data.id) && <AiOutlineCheck className="mt-3 mx-2 cursor-pointer" />
                }

                <p className="flex-1 bg-gray-50 py-2 mb-2 rounded-lg pl-2">{com.attributes.texte}</p>

            </div>
        </div>
    )

}

export default PostCommentaires