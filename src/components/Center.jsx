import { useEffect, useState } from 'react'
import PostForm from "./PostForm";
import Question from "./Question";
import axios from 'axios';
import { baseUrl } from '../api/globalVariable';


const Center = () => {

    let [questions, setQuestions] = useState();
    const token = localStorage.getItem('token')


    const fetchQuestions = async () => {
        const questionPost = await axios.get(baseUrl + 'post-questions', {
            headers: { "Authorization": `Bearer ${token}` }
        }).then(response => setQuestions(response.data.data))
            .catch(error => console.log(error))

    }

    useEffect(() => {
        // const token = localStorage.getItem('token')
        // axios.get(baseUrl + 'post-questions', {
        //     headers: { "Authorization": `Bearer ${token}` }
        // }).then(response => setQuestions(response.data)).catch(error => console.log(error))
        fetchQuestions();

    }, [])

    // let zavatra = Object.keys(questions).map((key) => [Number(key), questions[key]])


    //console.log(questions)
    return (
        <>

            <PostForm />
            <Question />
        </>
    );
}

export default Center