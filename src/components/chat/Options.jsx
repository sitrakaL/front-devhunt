import React from "react";

import "./Options.css";

const Options = (props) => {
  const options = [
    {
        text: "Javascript",
        handler: props.actionProvider.handleJavascriptList,
        id: 1,
      },
    { text: "API", handler: () => {}, id: 2 },
    { text: "Security", handler: () => {}, id: 3 },
  ];

  const optionsMarkup = options.map((option) => (
    <button
      className="learning-option-button"
      key={option.id}
      onClick={option.handler}
    >
      {option.text}
    </button>
  ));

  return <div className="learning-options-container">{optionsMarkup}</div>;
};

export default Options;