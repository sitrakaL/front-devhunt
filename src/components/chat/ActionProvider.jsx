import React from 'react';

const ActionProvider = ({ createChatBotMessage, setState, children }) => {
  const handleHello = () => {
    const botMessage = createChatBotMessage('Hello. Nice to meet you.');

    setState((prev) => ({
      ...prev,
      messages: [...prev.messages, botMessage],
    }));
  };

  const handleDog = () => {
    const botMessage = createChatBotMessage(
      "Here's a nice dog picture for you!",
      {
        widget: 'dogPicture',
      }
    );

    setState((prev) => ({
      ...prev,
      messages: [...prev.messages, botMessage],
    }));
  };

  const handleJavascriptList = () => {
    const message = createChatBotMessage(
        "Fantastic, I've got the following resources for you on Javascript:",
        {
          widget: "javascriptLinks",
        }
      );
  
      setState((prev) => ({
        ...prev,
        messages: [...prev.messages, message],
      }));
  }

  const test = () => {
    console.log('go tyyyh')
  }

  const handleThanks = () => {
    const message = createChatBotMessage(
        "Je vous en prie ! Je serai toujours là si vous avez d'autres questions à me poser.",
      );
  
      setState((prev) => ({
        ...prev,
        messages: [...prev.messages, message],
      }));
  }

  // Put the handleHello and handleDog function in the actions object to pass to the MessageParser
  return (
    <div>
      {React.Children.map(children, (child) => {
        return React.cloneElement(child, {
          actions: {
            handleHello,
            handleDog,
            handleJavascriptList, test,
            handleThanks
        },
        });
      })}
    </div>
  );
};

export default ActionProvider;