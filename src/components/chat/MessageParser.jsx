// in MessageParser.jsx
import React from 'react';

const MessageParser = ({ children, actions }) => {
    
  const parse = (message) => {
    const lowerCaseMessage = message.toLowerCase()
    if (lowerCaseMessage.includes('hello')) {
      actions.handleHello();
    }

    if (lowerCaseMessage.includes('dog')) {
      actions.handleDog();
    }

    if (lowerCaseMessage.includes('merci')) {
        actions.handleThanks();
      }
  };

  return (
    <div>
      {React.Children.map(children, (child) => {
        return React.cloneElement(child, {
          parse: parse,
          actions,
        });
      })}
    </div>
  );
};

export default MessageParser;