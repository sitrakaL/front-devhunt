import { useState, useEffect } from "react"
import axios from "axios"
import { baseUrl } from "../api/globalVariable"
import { composant } from "../api/globalVariable"
const SignIn = (props) => {

    const [identifier, setIdentifier] = useState('')
    const [password, setPassword] = useState('')
    const [invalid, setInvalid] = useState('')
    const [errors, setErrors] = useState({})

    const handleClick = () => {
        props.handleSignUp();
    }

    const validation_formulaire = () => {
        let error = {}
        let hasErrors = false
        if(identifier == ''){
          error.identifier = 'Vous devez renseigner votre identifiant !'
          hasErrors = true
        }
        if(password == ''){
            error.password = 'Vous devez entrer votre mot de passe !'
          hasErrors = true
        }
        if (hasErrors == false) {
          authentificate()
        }
        setErrors(error)
      }

    const authentificate = () => {
        let formData = new FormData()
        formData.append("identifier", identifier)
        formData.append("password", password)
    
        axios.post(baseUrl + 'auth/local', formData)
            .then(function(response){
                //implementena atao anaty localStorage ny session anle olona
                localStorage.setItem('token', response.data.jwt)
                localStorage.setItem('session', JSON.stringify(response.data.user))
                window.location.href = "/home"
                console.log(localStorage.getItem('token'))
            })
            .catch(function(error){
                setInvalid('Invalid identifier or password !')
                console.log('Invalid identifier or password')
            })
    }

    return (
        <div className="w-full max-w-xl">
            <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                <div className="mb-4">
                    <label className="block text-gray-700 text-sm font-bold mb-2" for="username">
                        Username
                    </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Username" autoComplete="off"
                    onChange={(event) => {setIdentifier(event.target.value); setInvalid(null)}}  />
                    <small className="small_alert text-red-600">{errors.identifier}</small>
                </div>
                <div className="mb-6">
                    <label className="block text-gray-700 text-sm font-bold mb-2" for="password">
                        Password
                    </label>
                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder="Password" autoComplete="off"
                    onChange={(event) => {setPassword(event.target.value); setInvalid(null)}}  />
                    <small className="small_alert text-red-600">{errors.password}</small>
                </div>
                <div className="mb-5 text-red-500 text-bold">{invalid}</div>
                <div className="flex items-center justify-between">
                    <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button"
                        onClick={() => {
                            validation_formulaire()
                        }}
                    >
                        Sign In
                    </button>
                    <a className="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800" href="#" onClick={handleClick}>
                        Sign Up
                    </a>
                    <a className="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800" href="#">
                        Forgot Password?
                    </a>
                </div>
            </form>
        </div>
    );
}

export default SignIn