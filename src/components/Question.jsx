import { AiOutlineLike } from "react-icons/ai"
import { baseUrl } from "../api/globalVariable";
import { AiOutlineMinus, AiOutlinePlus, AiOutlineCheck } from "react-icons/ai";
import { useState, useEffect, useRef } from "react";
import axios from "axios";
import { BsThreeDots, BsDot } from "react-icons/bs"
import Swal from "sweetalert2";

const Question = (props) => {
    const [disable, setDisable] = useState(true)
    const [showModal, setShowModal] = useState(false);
    const [title, setTitle] = useState('')
    const [questions, setQuestions] = useState('')
    const [inputValue, setInputValue] = useState("");
    const [filteredOptions, setFilteredOptions] = useState([]);
    const [commentText, setCommentText] = useState('')
    const [posts, setPosts] = useState([])
    const [coms, setComs] = useState([])
    const [userPosts, setUserPosts] = useState([]);
    const [inputComs, setInputComs] = useState("");
    const [commentNom, setCommentNom] = useState("")
    const [commentPrenom, setCommentPrenom] = useState("")
    const [commentUserId, setCommentUserId] = useState("")
    const [modifText, setModifText] = useState("")
    const [modifTitle, setModifTitle] = useState('')
    const [modifSolved, setModifSolved] = useState(false)
    const [modifId, setModifId] = useState(0)
    const [modifComments, setModifComments] = useState([])
    const [modifTags, setModifTags] = useState('')
    const [modifUser, setModifUser] = useState('')

    const sess = JSON.parse(localStorage.getItem('session'))
    const user_id = sess.id

    // getting post by the sess_id
    const getPostByUserId = () => {
        axios.get(baseUrl + `users/${user_id}?populate=*`)
            .then(response => {
                // console.log(response.data.post_ids);
                setUserPosts(response.data.post_ids)
            })
            .catch(error => {
                console.error(error);
            });
    }

    // getting all posts

    // compare the all posts to the users posts:
    // if post in all posts match the user post, add check box, otherwise don't put checkbox



    const commenter = (key) => {

        axios.post(baseUrl + 'commentaires', {
            data: {
                texte: inputComs,
                score: 0,
                post_id: key,
                post_answer: null,
                check_mark: false,
                user_id: JSON.parse(localStorage.getItem('session')).id
            }
        }, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(response => {

        //
            })
            .catch(error => {
                console.error(error);
            });
        // console.log(JSON.parse(localStorage.getItem('session')).id)
        // window.location.reload();
    }

    const getPostQuestions = () => {
        axios.get(baseUrl + 'post-questions?populate=deep,2', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(response => {
                let myArray = [];
                response.data.data.forEach(element => {
                    console.log('id : '+element.attributes.user_id.data.id)
                    let newObj = {
                        id: element.id,
                        title: element.attributes.title,
                        body: element.attributes.body,
                        solved: false,
                        createdAt: element.attributes.createdAt,
                        updatedAt: element.attributes.updatedAt,
                        commentaires: element.attributes.commentaire_id.data,
                        // userId: element.attributes.user_id.data.id,
                        commmentUserId: commentUserId,
                        commentNom: commentNom,
                        commentPrenom: commentPrenom
                    }

                    for (const key in newObj.commentaires) {
                        axios.get('http://ec2-52-73-37-109.compute-1.amazonaws.com:1337/api/users?filters[$and][0][commentaire_id][id][$eq]=' + newObj.commentaires[key].id + '&populate=deep', {
                            headers: {
                                Authorization: `Bearer ${localStorage.getItem('token')}`
                            }
                        })
                            .then(response => {
                                newObj.commentaires[key].commentPrenom = response.data[0].prenom
                                newObj.commentaires[key].commentUserId = response.data[0].id
                                newObj.commentaires[key].commentNom = response.data[0].nom
                            })
                            .catch(error => {
                                console.error(error);
                            });
                    }
                    console.log('topic  : ' + newObj.commentaires)
                    // newObj.commentaires.forEach((element) => {
                    //     getUserComment(element.id)
                    //     // let userComment = {
                    //     //     nom : commentUserId,
                    //     //     prenom : commentPrenom,
                    //     //     id : commentUserId
                    //     // }
                    //     // UserCommentArray.push}(userComment)
                    //     newObj.commentaires.commentUserId = commentUserId
                    // })

                    myArray.push(newObj)
                });
                setPosts(myArray)
                // console.log(response.data.data)
            })
            .catch(error => {
                console.error(error);
            });
    }

    const commentaire = (id) => {
        axios.get(baseUrl + 'commentaires/' + id + '?populate=deep,5', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(response => {
                // console.log(response.data.data.attributes.user_id.data.attributes.nom)
                return (response.data.data.attributes.user_id.data.attributes.nom)
            })
            .catch(error => {
                console.error(error);
            });
    }

    const handleButtonClick = () => {
        setInputComs("");
    };

    const getCommentaires = () => {
        axios.get(baseUrl + 'commentaires?populate=deep,5', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(response => {
                let myArray = [];
                response.data.data.forEach(element => {
                    let newObj = {
                        id: element.id,
                        score: element.attributes.title,
                        check_mark: false,
                        texte: element.attributes.texte,
                        createdAt: element.attributes.createdAt,
                        updatedAt: element.attributes.updatedAt,
                    }
                    myArray.push(newObj)
                });
                setComs(myArray)
                // console.log(response.data)
                // console.log(coms)
            })
            .catch(error => {
                console.error(error);
            });
    }

    const supprimerPost = () => {

    }

    const modifierPost = (id) => {
        axios.get(baseUrl + 'post-questions/' + id).then(function (response) {
            // setModifTitle(response.data.attributes.title)
            // setModifComments(response.data.attributes.commentaire_id)
            // setModifUser(response.data.attributes.user)
            // setModifTag(response.data.attributes.tag)
            // setModifSolved(response.data.attributes.solved)
            axios.put(baseUrl + 'post-questions/' + id, {
                data: {
                    title: modifTitle,
                    body: modifText,
                    commentaire_id: response.data.attributes.commentaire_id,
                    solved: modifSolved,
                    user: modifUser,
                    tag: modifTag
                }
            }, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then(response => {
                    console.log('Text :', modifText)
                    console.log('title :', modifTitle)
                    console.log('solved :', modifSolved)
                    console.log('id :', modifId)
                    console.log(response.data)
                })
                .catch(error => {
                    console.error(error);
                });
        })

    }

    // useEffect(() => {
    //     getPostByUserId()
    // }, [])

    useEffect(() => {
        getPostQuestions()
    }, [posts]);

    useEffect(() => {
        getCommentaires()
    }, []);

    return (
        <>
            {/* <button onClick={() => { getPostQuestions() }}>Recevoir Users</button> */}

            {
                showModal ? (
                    <>
                        <div
                            className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                        >
                            <div className="relative w-full my-6 max-w-2xl">
                                {/*content*/}
                                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                    {/*header*/}
                                    <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                        <h3 className="text-3xl font-semibold">
                                            Modifier votre question...
                                        </h3>
                                        <button
                                            className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                                            onClick={() => setShowModal(false)}
                                        >
                                            <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                                                ×
                                            </span>
                                        </button>
                                    </div>
                                    {/*body*/}
                                    <div className="relative p-6 flex-auto">
                                        <label className="block text-gray-700 text-sm font-bold mb-2 py-2" for="username">
                                            Titre
                                        </label>
                                        <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="titre" type="text" placeholder="Username" autoComplete="off"
                                            value={modifTitle}
                                            disabled />
                                        <label className="block text-gray-700 text-sm font-bold mb-2 py-2" for="username">
                                            Contenu
                                        </label>
                                        <textarea id="message" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500" placeholder="Ecrivez ici"
                                            value={modifText} onChange={(event) => { setModifText(event.target.value) }}></textarea>
                                    </div>
                                    {/*footer*/}
                                    <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                        <button
                                            className="text-red-500 background-transparent px-6 py-2 text-sm focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                            type="button"
                                            onClick={() => { setShowModal(false); }}
                                        >
                                            Fermer
                                        </button>
                                        <button
                                            className="bg-blue-500 text-white active:bg-emerald-600 text-sm px-3 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                            type="button"
                                            onClick={() => { setShowModal(false); modifierPost(modifId); }}
                                        >
                                            Valider
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                    </>
                ) : null
            }

            {
                posts.map(post => (
                    <div className="flex flex-col bg-white mx-2 py-5 px-5 rounded-md my-2 shadow-md">
                        {/** Title */}

                        <div className="flex">
                            <div className="my-1 flex-1">
                                {
                                    post.solved
                                        ? <h1 className="text-3xl my-4 font-bold"><h1 className="text-green-600">[RESOLU]</h1>{post.title}</h1>
                                        : <h1 className="text-3xl my-4 font-bold">{post.title}</h1>
                                }
                            </div>
                            <div className="flex-3 mt-8 text-2xl"><button className="bg-gray-300 py-2 px-2 rounded-2xl" ><BsThreeDots /></button></div>
                        </div>

                        {/** Profile pic, tags */}
                        <div className="flex my-5">
                            <div className="flex-none w-20 align-center ">
                                <img className="object-fill w-15 h-full rounded-full" src="https://media.techtribune.net/uploads/2020/07/one-piece-luffy-haki-training-wano-1228926-1280x0.jpeg" />
                            </div>
                            <div className="flex flex-col flex-initial w-full ml-1">
                                <div className="font-bold">Elisabeth May </div>
                                <div className="">{post.createdAt}</div>
                                {
                                    post.userId == JSON.parse(localStorage.getItem('session')).id
                                        ? <div className="flex gap-2">
                                            <a className="text-blue-600  underline" href="#" onClick={() => {
                                                setShowModal(true)
                                                setModifText(post.body)
                                                setModifTitle(post.title)
                                                setModifId(post.id)
                                            }}>Editer</a>
                                            <a className="text-gray-600 underline" href="#">Supprimer</a>
                                        </div>
                                        : <></>
                                }

                            </div>
                        </div>
                        <hr />
                        {/** Explicit question */}
                        <div className="my-4">
                            <p>
                                {post.body}
                            </p>
                        </div>

                        {/** Comments */}
                        <div className="flex">
                            <div className="bg-white w-full">
                                <div className="container flex flex-col justify-center px-2 py-1">
                                    <div className="space-y-4">
                                        <details className="w-full rounded-lg ring-1 ring-purple-600 px-3 py-1">
                                            <summary className="px-2 py-3 cursor-pointer">
                                                Commenter
                                            </summary>

                                            {post.commentaires.map(com => (
                                                <div className="flex flex-col my-2">
                                                    <div className="flex flex-row">
                                                        <p className="font-bold pl-2"> {com.commentPrenom} {com.commentNom} </p>
                                                        <BsDot className="mt-1 ml-2" />
                                                        <p className="flex-1 text-gray-500 pl-2">{com.attributes.createdAt}</p>
                                                        <div className="flex gap-2 mt-1">
                                                            <AiOutlineMinus className="cursor-pointer" />
                                                            <AiOutlinePlus className="cursor-pointer" />
                                                        </div>
                                                    </div>
                                                    <div className="flex">
                                                        <AiOutlineCheck className="mt-3 mx-2 cursor-pointer" />
                                                        <p className="flex-1 bg-gray-50 py-2 mb-2 rounded-lg pl-2">{com.attributes.texte}</p>

                                                    </div>
                                                </div>
                                            ))}

                                            <textarea id="message" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500" value={inputComs} placeholder="Votre réponse..."
                                                onChange={(event) => { setInputComs(event.target.value) }}></textarea>
                                            <button className="mt-3 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                                onClick={() => { commenter(post.id) ; 
                                                    Swal.fire({
                                                        icon: 'success',
                                                        title: 'Commentaire enregistré',
                                                        text: 'Votre commentaire a été bien ajouté au post !',
                                                        showConfirmButton: true
                                                      })
                                                }}>
                                                Valider
                                            </button>
                                        </details>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                ))}
        </>
    );

}

export default Question