import { createContext, useState } from 'react'
import './App.css'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Authentification from './pages/Authentification';
import Home from './pages/Home';
import SignIn from './components/SignIng';
import SignUp from './components/SignUp';
import Chatbot from './components/chat/ChatBot'
import Profile from './pages/Profile'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Authentification />}></Route>
        <Route path='/chat' element={<Chatbot />}></Route>
        <Route path='/profile' element={<Profile />}></Route>
        <Route path='/home' element={<Home />}></Route>
        <Route path='/sign-in' element={<SignIn />}></Route>
        <Route path='/sign-up' element={<SignUp />}></Route>
      </Routes>
    </BrowserRouter>
  )
}


export default App
