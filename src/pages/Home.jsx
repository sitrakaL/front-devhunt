import NavBar from "../components/NavBar"
import Left from "../components/Left"
import Center from "../components/Center"
import Right from "../components/Right"

const Home = () => {
    return (
        <>
            <NavBar />
            <div className="flex relative mt-20">
                <div className="flex-2 ml-4 bg-white rounded-lg h-full w-2/5">
                    <Left />
                </div>
                <div className="flex-2 w-full ml-5 mr-2"><Center /></div>
                <div className="flex-1 mr-80 ml-5 relative rounded-lg fixed h-full w-2/3">

                    <div className="fixed"><Right /></div>

                </div>
            </div>
        </>
    )
}

export default Home