import Test from "../components/test";
import SignIn from "../components/SignIng";
import SignUp from "../components/SignUp";
import { baseUrl } from "../api/globalVariable";
import { useState } from "react";

const Authentification = () => {
    const [isSignup, setIsSignup] = useState(false)
    const handleSignUp = () => {
        setIsSignup(!isSignup)
    }
    return (
        <>
          <div class="flex flex-col justify-center h-screen">
            <div class="flex justify-center">
            <div className="card rounded-2xl bg-white w-full lg:max-w-5xl sm:max-w-3xl p-2">
                <div class="grid grid-cols-5 grid-flow-row gap-2">
                    <div class="rounded-2xl col-span-2">
                        <div className="card text-white rounded-2xl container bg-blue-500 max-w-3xl mx-auto p-10 h-full">
                            <div className="">
                                UC-TEAM
                            </div> <br />
                            <div className="mt-10 font-bold text-4xl">
                                Start your journey with us.
                            </div>
                            <div className="mt-3">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.
                            </div>
                            <div className="card bg-purple-800 rounded-2xl p-3 mt-10">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unk
                            </div>
                        </div>
                    </div>
                    <div class="col-span-3 flex flex-col justify-center h-full">
                        <div className="card container rounded-2xl max-w-3xl mx-auto p-5">
                            {
                                isSignup 
                                    ? <SignUp handleSignUp={handleSignUp}/>
                                    : <SignIn handleSignUp={handleSignUp} />
                            }
                            
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        </>
    );
}

export default Authentification